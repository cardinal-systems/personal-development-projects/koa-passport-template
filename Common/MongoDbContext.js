const mongoose = require("mongoose");
const Schema = require("./Schemas/schema");
const Logger = require("./Logger");

class MongoDbContext extends Schema {
    
    constructor()
    {
        super();
        
        this.logger = new Logger("MongoDbContext")
        
        this.reconnectCount = 1;
        this.isOffline = false;
        this.client = Promise.resolve(this.setupClient());
    }

    async setupClient() 
    {
        await mongoose.connect(process.env.MONGO_DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true, 
            useFindAndModify: false
        }, (err) => {
            if (err) {
                this.isOffline = true;
                this.logger.Error(`[Attempt: ${this.reconnectCount}] Connection Error: ${err}`);
                this.reconnectCount++;
            } else {
                this.isOffline = false;
                this.reconnectCount = 1;
                this.logger.Log("Connected to MongoDB.");
            }
        });

        if (this.isOffline && this.reconnectCount < 4) {
            await this.setupClient();
        }

        return mongoose.connection;
    }

    async disconnect() {
        mongoose.disconnect((err) => {
            this.logger.Error(err);
        });
        this.logger.Log("Disconnected from MongoDB.");
    }
}

module.exports = {
    MongoDbContext
}