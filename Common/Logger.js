const moment = require("moment");

class Logger {
    constructor(serviceLocation) {
        this.serviceLocation = serviceLocation || "App";
    }

    TimeStamp(date) {
        return moment(date).format("DD-MM-YY HH:mm:ss");
    }

    Log(message) {
        console.debug("\x1b[37m%s",`${this.TimeStamp()} - LOG - ${this.serviceLocation} - ${message}`,"\x1b[37m");
    }

    Verbose(message) {
        if (process.env.LOG_LEVEL == "verbose") {
            console.debug("\x1b[34m%s",`${this.TimeStamp()} - INFO - ${this.serviceLocation} - ${message}`,"\x1b[37m");
        }
    }

    Debug(message) {
        if (process.env.LOG_LEVEL == "verbose" || process.env.LOG_LEVEL == "debug") {
            console.debug("\x1b[32m%s",`${this.TimeStamp()} - DEBUG - ${this.serviceLocation} - ${message}`,"\x1b[37m");
        }
    }

    Warn(message) {
        if (process.env.LOG_LEVEL != "error") {
            console.warn("\x1b[33m%s",`${this.TimeStamp()} - WARNING - ${this.serviceLocation} - ${message}`,"\x1b[37m");
        }
    }

    Error(message) {
        console.error("\x1b[31m%s",`${this.TimeStamp(Date.now())} - ERROR - ${this.serviceLocation} - ${message}`,"\x1b[37m");
    }
}

module.exports = Logger;