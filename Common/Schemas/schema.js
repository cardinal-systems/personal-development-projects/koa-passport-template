const mongoose = require("mongoose");

// Schemas
const UserSchema = require("./UserSchema");

class Schema 
{
    constructor() 
    {
        this.schema = mongoose.Schema;
    }

    Users() 
    {
        return new UserSchema();
    }
}

module.exports = Schema;