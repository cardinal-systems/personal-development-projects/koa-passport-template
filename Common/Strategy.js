const Passport = require("koa-passport");

const LocalAuthenticationStrategy = require("passport-local").Strategy;
const LocalRegistrationStrategy = require("passport-custom").Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
const TwitterStrategy = require("passport-twitter").Strategy;

const Logger = require("./Logger");

class Strategy {

    constructor(connection)
    {
        this.connection = connection;
        this.logger = new Logger("Strategy");

        this.logger.Verbose("Setting up SerializeUser");
        this.SerializeUser();
        this.logger.Verbose("Finished setup of SerializeUser");

        this.logger.Verbose("Setting up DeserializeUser");
        this.DeserializeUser();
        this.logger.Verbose("Finished setup of DeserializeUser");

        this.logger.Verbose("Setting up Local Authentication & Registration");
        this.LocalAutentication();
        this.LocalRegistration();
        this.logger.Verbose("Finished setup of Local Authentication & Registration");

        this.logger.Verbose("Setting up Facebook Authentication");
        this.FacebookAuthentication();
        this.logger.Verbose("Finished setup of Facebook Authentication");

        this.logger.Verbose("Setting up Google Authentication");
        this.GoogleAuthentication();
        this.logger.Verbose("Finished setup of Google Authentication");

        this.logger.Verbose("Setting up Google Authentication");
        this.TwitterAuthentication();
        this.logger.Verbose("Finished setup of Google Authentication");
    }

    SerializeUser()
    {
        Passport.serializeUser((user, done) => {
            done(null, user._id)
        });
    }

    DeserializeUser()
    {
        var me = this;
        Passport.deserializeUser(async (id, done) => {
            await me.connection.Users().findById(id, done);
        });
    }

    LocalAutentication() {
        var me = this;
        Passport.use("local", new LocalAuthenticationStrategy({
            usernameField: "username",
            passwordField: "password"
        }, (username, password, done) => {
                var users = me.connection.Users();
                return users.findOne({ username: username, google_id : { $exists : false }, facebook_id : { $exists : false }, twitter_id : { $exists : false } }, (err, user) => {
                    if (err) { return done(err); }
                    if (!user) {
                        return done("Invalid Username or Password", null); 
                    }
                    if (!user.validPassword(password, user.password)) {
                        return done("Invalid Username or Password", null); 
                    }
                    if (user && user.deleted) {
                        return done("Account has been deleted.", null); 
                    }
                    return done(null, user);
                });
            }
        ));
    }

    LocalRegistration() {
        var me = this;
        Passport.use("register", new LocalRegistrationStrategy((req, done) => {
            var request = req.body;
            var users = me.connection.Users();
            users.findOne({ username: request.username }, async (err, user) => {
                if (err) { me.logger.error(err); }
                if (user) { return done("Username already exists.", null); }
                if (request.password == request.confirm_password) {
                    return done("Passwords do not match.", null);
                }
    
                var newUser = new users({
                    display_name: request.username,
                    username: request.username,
                    email: request.email
                });
    
                newUser.password = newUser.generateHash(request.password);
    
                return await newUser.save(function(err) {
                    if (err) {
                        return done(err, null); 
                    }
                    return done(null, newUser);
                });
            });
        }));
    }

    FacebookAuthentication() {
        var me = this;
        Passport.use("facebook", new FacebookStrategy({
            clientID: `${process.env.FACEBOOK_CLIENT_ID}`,
            clientSecret: `${process.env.FACEBOOK_CLIENT_SECRET}`,
            callbackURL: "/Account/Login/Facebook/Callback"
        },
        async (accessToken, refreshToken, profile, done) => {
            var users = me.connection.Users();
            await users.findOne({ facebook_id: profile.id }, async (err, user) => {
                if (err) { 
                    this.logger.Error(`Error logging into Facebook: ${err}`);
                    return done(err); 
                }
                if (!user) {
                    var newUser = new users({
                        username: profile.username,
                        display_name: profile.displayName,
                        facebook_id: profile.id
                    });
                    return await newUser.save(function(err) {
                        if (err) {
                            this.logger.Error(`Error saving Facebook user: ${err}`);
                            return done(err);
                        }
                        return done(err, newUser);
                    });
                }
                return done(err, user);
            });
        }));
    }

    GoogleAuthentication() {
        var me = this;
        return Passport.use("google", new GoogleStrategy({
            clientID: `${process.env.GOOGLE_CLIENT_ID}`,
            clientSecret: `${process.env.GOOGLE_CLIENT_SECRET}`,
            callbackURL: "/Account/Login/Google/Callback"
        },
        async (token, tokenSecret, profile, done) => {
            var users = me.connection.Users();
            await users.findOne({ google_id: profile.id }, async (err, user) => {
                if (err) { 
                    this.logger.Error(`Error logging into Google: ${err}`);
                    return done(err); 
                }
                if (!user) {
                    var newUser = new users({
                        username: profile.emails[0].value,
                        display_name: profile.displayName,
                        google_id: profile.id
                    });
                    return await newUser.save(function(err) {
                        if (err) { 
                            this.logger.Error(`Error saving Google user: ${err}`);
                            return done(err); 
                        }
                        return done(err, newUser);
                    });
                }
                return done(err, user);
            });
        }));
    }

    TwitterAuthentication() {
        var me = this;
        Passport.use("twitter", new TwitterStrategy({
            consumerKey: `${process.env.TWITTER_CLIENT_ID}`,
            consumerSecret: `${process.env.TWITTER_CLIENT_SECRET}`,
            callbackURL: "/Account/Login/Twitter/Callback"
        },
        async (token, tokenSecret, profile, done) => {
            var users = me.connection.Users();
            await users.findOne({ twitter_id: profile.id }, async (err, user) => {
                if (err) { 
                    this.logger.Error(`Error logging into Twitter: ${err}`);
                    return done(err); 
                }
                if (!user) {
                    var newUser = new users({
                        username: profile._json.username,
                        display_name: profile._json.name,
                        twitter_id: profile.id
                    });
                    return await newUser.save(function(err) {
                        if (err) { 
                            this.logger.Error(`Error saving Twitter user: ${err}`);
                            return done(err); 
                        }
                        return done(err, newUser);
                    });
                }
                return done(err, user);
            });
          }
        ));
        TwitterStrategy
    }
}

module.exports = Strategy;