// #region 
const path = require("path");

const Koa = require("koa");
const KoaLogger = require("koa-logger");
const EjsRender = require("koa-ejs");
const BodyParser = require("koa-bodyparser");
const Cors = require("@koa/cors");
const Helmet = require("koa-helmet");
const Passport = require("koa-passport");
const Router = require("koa-router");
const Session = require("koa-session");
const Static = require("koa-static");

// Common Imports
const Logger = require("./Common/Logger");

// #endregion

class App {
    constructor()
    {
        this.server = new Koa();
        this.router = new Router();
        this.logger = new Logger("App");
    }

    ConfigureApplication() {
        this.logger.Verbose("Configuring Koa Helmet");
        this.server.use(Helmet());
        this.logger.Verbose("Configured Koa Helmet");

        if (process.env.NODE_ENV === "verbose") {
            this.logger.Verbose("Configuring Koa Logger");
            this.server.use(KoaLogger())
            this.logger.Verbose("Configured Koa Logger");
        }

        this.logger.Verbose("Configuring Body Parser");
        this.server.use(BodyParser());
        this.logger.Verbose("Configured Body Parser");

        this.logger.Verbose("Configuring CORS");
        this.server.use(Cors());
        this.logger.Verbose("Configured CORS");

        this.logger.Verbose("Configuring Session");
        this.server.keys = [`${process.env.SECRET}`];
        this.server.use(Session({}, this.server));
        this.logger.Verbose("Configured Session");

        this.logger.Verbose("Configuring Passport");
        this.server.use(Passport.initialize());
        this.server.use(Passport.session());
        this.logger.Verbose("Configured Passport");

        this.logger.Verbose("Configuring Static");
        this.server.use(Static("./Public"));
        this.logger.Verbose("Configured Static");

        this.logger.Verbose("Configuring Ejs");
        EjsRender(this.server, {
            root: path.join(__dirname, "Views"),
            layout: "./Shared/_Layout",
            viewExt: "html",
            debug: process.env.NODE_ENV === "development"
        })
        this.logger.Verbose("Configured Ejs");
    }

    MountApplicationRoutes()
    {
        this.logger.Verbose("Mounting Application Routes");
        require("./Controllers/controllers")(this.router);
        this.server.use(this.router.routes());
        this.server.use(this.router.allowedMethods());
        this.logger.Verbose("Mounted Application Routes");
    }

    MountApplicationCustomErrors()
    {
        this.server.use(async (ctx, next) => {
            try {
                await next();
                if (ctx.status === 404) {
                    ctx.throw(404);
                }
            } catch (err) {
                ctx.status = err.status || 500;
                this.logger.Error(`${err.status} - ${err.message} - Requesting ${ctx.url}`);
                await ctx.render("/Shared/_Error", { errors: [err.message] });
            }
          })        
    }
}

module.exports = (isTestMode) => {
    if (!isTestMode) {
        var app = new App();
        app.logger.Verbose("Configuring Application");
        app.ConfigureApplication();
        app.MountApplicationRoutes();
        app.MountApplicationCustomErrors();
        app.logger.Verbose("Configured Application Successfully");
        return app.server;
    } 
    else {
        return new App();
    }
}