const Passport = require("koa-passport");
const Router = require("koa-router");

const { AuthenticationMethods } = require("../Common/AuthenticationMethods");
const Logger = require("../Common/Logger");

class HomeController {
    constructor(connection) 
    {
        this.connection = connection;
        this.router = new Router();
        this.logger = new Logger("HomeController");

        this.authenticationMethods = AuthenticationMethods();

        this.InitialiseEndpoints();
    }

    async AuthoriseEndpoint(ctx, next) {
        return this.authenticationMethods.length > 0 ? ( ctx.isAuthenticated() ? await next() : ctx.redirect("/Account/Login") ) : await next();
    }

    InitialiseEndpoints() {
        this.router.get("/", (ctx, next) => this.AuthoriseEndpoint(ctx, next), async (ctx) => { 
            ctx.status = 200;
            await ctx.render("Home/Index", {user: ctx.state.user });
        });
    }
}

module.exports = (connection) => {
    var homeController = new HomeController(connection);
    return homeController.router.routes();
}