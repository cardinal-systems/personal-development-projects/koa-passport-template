const { MongoDbContext } = require("../Common/MongoDbContext");
const Strategy = require("../Common/Strategy");

class Controllers {
    constructor(router) 
    {
        this.connection = new MongoDbContext();
        this.strategy = new Strategy(this.connection);
        this.router = router;
        
        this.InitialiseEndpoints();
    }

    InitialiseEndpoints() 
    {
        this.router.get("/", async (ctx) => { 
            ctx.status = 302; 
            ctx.redirect("/Home");
        });
        
        this.router.use("/Home", require("./HomeController")(this.connection));
        this.router.use("/Account", require("./AccountController")(this.connection));
        this.router.use("/Profile", require("./ProfileController")(this.connection));
    }
}


module.exports = (router) => {
    new Controllers(router);
}