const Router = require("koa-router");
const Logger = require("../Common/Logger");

class AccountController {
    constructor(connection) 
    {
        this.connection = connection;
        this.router = new Router();
        this.logger = new Logger("AccountController");

        this.InitialiseEndpoints();
    }

    InitialiseEndpoints() {
        this.router.use("/Login", require("./LoginController")(this.connection));
        
        this.router.use("/Register", require("./RegisterController")(this.connection));

        this.router.get("/Logout", async (ctx) => {
            if (AuthenticationMethods().length > 0) {
                ctx.status = 200;
                await ctx.render("Account/Logout");
            } else {
                ctx.status = 302;
                ctx.redirect("/Home");
            }
        });

        this.router.post("/Logout", async (ctx) => {
            await ctx.logout();
            ctx.status = 302;
            ctx.redirect("/Home");
        });
    }
}

module.exports = (connection) => {
    var accountController = new AccountController(connection);
    return accountController.router.routes();
}