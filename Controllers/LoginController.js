const Router = require("koa-router");
const Passport = require("koa-passport");
const Logger = require("../Common/Logger");

const { AuthenticationMethods } = require("../Common/AuthenticationMethods");

class LoginController {
    constructor(connection) 
    {
        this.connection = connection;
        this.router = new Router();
        this.logger = new Logger("LoginController");

        this.InitialiseEndpoints();
    }

    InitialiseEndpoints() {

        this.router.get("/", async (ctx) => {
            if (AuthenticationMethods().length > 0) {
                ctx.status = 200;
                await ctx.render("Account/Login", {errors: []});
            } else {
                ctx.status = 302;
                ctx.redirect("/Home");
            }
        });

        if (process.env.IS_LOCAL_AUTHENTICATION == 1) {
            this.router.post("/Local", async (ctx) => {
                return Passport.authenticate("local", async (err, user) => {
                    if (user) {
                        ctx.status = 302;
                        this.logger.Debug(user);
                        await ctx.login(user._id);
                        ctx.redirect("/Home");
                    } else {
                        ctx.status = 401;
                        this.logger.Error(err);
                        await ctx.render("/Account/Login", {errors: err ? [err] : [`${ctx.status} - ${ctx.message}`]});
                    }
                })(ctx);
            });
        }

        if (process.env.IS_FACEBOOK_AUTHENTICATION == 1) {
            this.router.get("/Facebook", Passport.authenticate("facebook"));

            this.router.get("/Facebook/Callback", (ctx) => {
                return Passport.authenticate("facebook", async (err, user) => {
                    if (user) {
                        ctx.status = 302;
                        this.logger.Debug(user);
                        await ctx.login(user._id);
                        ctx.redirect("/Home");
                    } else {
                        ctx.status = 401;
                        this.logger.Error(err);
                        await ctx.render("/Account/Login", {errors: err ? [err] : [`${ctx.status} - ${ctx.message}`]});
                    }
                })(ctx);
            })
        }

        if (process.env.IS_GOOGLE_AUTHENTICATION == 1) {
            this.router.get("/Google", Passport.authenticate("google", {scope: ["profile", "email"]}));
    
            this.router.get("/Google/Callback", (ctx) => {
                return Passport.authenticate("google", async (err, user) => {
                    if (user) {
                        ctx.status = 302;
                        this.logger.Debug(user);
                        await ctx.login(user._id);
                        ctx.redirect("/Home");
                    } else {
                        ctx.status = 401;
                        this.logger.Error(err);
                        await ctx.render("/Account/Login", {errors: err ? [err] : [`${ctx.status} - ${ctx.message}`]});
                    }
                })(ctx);
            });
        }

        if (process.env.IS_TWITTER_AUTHENTICATION == 1) {
            this.router.get("/Twitter", Passport.authenticate("twitter"));
    
            this.router.get("/Twitter/Callback", (ctx) => {
                return Passport.authenticate("twitter", async (err, user) => {
                    if (user) {
                        ctx.status = 302;
                        this.logger.Debug(user);
                        await ctx.login(user._id);
                        ctx.redirect("/Home");
                    } else {
                        ctx.status = 401;
                        this.logger.Error(err);
                        await ctx.render("/Account/Login", {errors: err ? [err] : [`${ctx.status} - ${ctx.message}`]});
                    }
                })(ctx);
            });
        }
    }

}

module.exports = (connection) => {
    var accountController = new LoginController(connection);
    return accountController.router.routes();
}