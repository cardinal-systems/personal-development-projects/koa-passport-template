require("dotenv").config();
const server = require("./server")(process.env.IS_TEST_MODE);
server.listen(process.env.PORT || 4000);